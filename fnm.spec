%define debug_package %{nil}

Name:           fnm
Version:        1.38.1
Release:        0%{?dist}
Summary:        Fast and simple Node.js version manager, built in Rust
Group:          Applications/System
License:        GPLv3+
URL:            https://github.com/Schniz/fnm
Source0:        %{url}/releases/download/v%{version}/fnm-linux.zip

%description
Fast and simple Node.js version manager, built in Rust

%global debug_package %{nil}

%prep
%autosetup -n %{name} -c

%install
install -d -m 755 %{buildroot}%{_bindir}
install -p -m 755 %{name} %{buildroot}%{_bindir}

%files
# %license LICENSE
# %doc docs *.md
%{_bindir}/%{name}

%changelog
* Mon Dec 09 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 
* Mon Jun 10 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 
* Tue May 28 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 
* Thu Aug 31 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 1.35.1

* Wed Jul 26 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 1.35.0

* Wed Dec 14 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 1.33.1

* Fri Nov 25 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 1.32.0

* Sun Sep 11 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Inital RPM

